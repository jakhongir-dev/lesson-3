// Lesson 3
function fizzBuzz() {
  const arr = [];
  const push = (str) => arr.push(str);

  for (let i = 1; i <= 100; i++) {
    const three = i % 3 == 0;
    const five = i % 5 == 0;

    if (three && five) push("FizzBuzz");
    else if (three) push("Fizz");
    else if (five) push("Buzz");
    else push(i);
  }
  console.log(arr);
}
fizzBuzz();

function filterArray(list) {
  const flatted = new Set(
    list
      .flat()
      .filter((el) => typeof el == "number")
      .sort((a, b) => a - b)
  );
  return Array.from(flatted);
}

const letList = [
  [2],
  23,
  "dance",
  [7, 8, 9, 7],
  true,
  [3, 5, 3],
  [777, 855, 9677, 457],
];
console.log(filterArray(letList)); // 2,3,5,7,8,9
